FROM selenium/node-chrome:89.0

USER root

ENV HOME=/root

RUN apt-get -y update \
    && apt-get -y install build-essential openssh-client openssh-server \
    && curl -fsSL https://deb.nodesource.com/setup_current.x | bash - \
    && apt-get install -y nodejs && npm install --global @angular/cli \
    && npm install --global yarn # buildkit
